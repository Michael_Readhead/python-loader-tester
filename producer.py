import logging
from multiprocessing import Process
import sched
import time


class Producer(Process):
    """
    A seperate process for populating the work queue.

    Loads config, then using a scheduler sends requests into a
    queue for sending to a remote host.
    Should isolate the i/o from other conerns ensuring maximum through put.
    """

    def __init__(self, queue=None, config=None):
        """
         Construcutor - loads config initialises scheduler and logging.

         Originally planned to be Producer per host+path combo.



        :param queue: For adding work to for later work.
        :param config: Config - loaded from py file, in dict format.
                        (Simple to change to yaml or json, but py works)
        """
        super().__init__()
        self.shutdown_queue = None
        self.work_queue = queue
        self.load_config(config)
        self.scheduler = sched.scheduler(time.time, time.sleep)
        self.logger = logging.getLogger("load_test_producer")

    def load_config(self, config):
        """
        Not happy with the arrangment of the config, but it does work.

        `req_per_sec` feels like it's in the wrong place.

        :param config: Config in dict.
                Format:
                {
                    "host/path" : {
                        "headers": (currently auth - could be anything.)
                        "req_per_sec": - Requests per second,
                        "method": HTTP METHOD i.e GET, POST
                        "payload":... (may include dynamic fields...)
                        "response": expected response for comparison
                    }
                }
        """
        for path, config in config.items():
            req_per_sec = config.get("req_per_sec")
            payload = config.get("payload")
            response = config.get("response")
            request = {
                "method": config.get("method"),
                "path": path,
                "headers": config.get("headers"),
                "payload": payload
            }
            self.req_per_sec = req_per_sec
            self.request = request
            self.response = response

    def enter_request(self):
        """
        Add request to queue, schedule the next request.

        Recusive (ish) - schedules itself to be called again.
        """
        self.logger.debug("Adding request")
        self.work_queue.put(
            (self.request, self.response)
        )
        self.scheduler.enter(1, 1, self.enter_request)

    def shutdown(self):
        """
        Tidy up any unfired requests.
        """
        self.work_queue.close()
        for event in self.scheduler.queue:
            self.scheduler.cancel(event)

    def run(self):
        """
        Main loop, schedules as many requests as needed in 1 second as needed.

        Relies on recursion in enter request to reschedule.

        """
        self.logger.info("Running...")
        for i in range(self.req_per_sec):
            self.scheduler.enter(1, 1, self.enter_request)
        try:
            self.scheduler.run()
        except KeyboardInterrupt:
            self.logger.info("Exiting..")
            self.shutdown()
            self.close()
