import asyncio
import logging
from multiprocessing import Process
import time

import httpx
from httpx import Request
from httpx import AsyncClient

from config import DYNAMIC_MAP


class LoadTestHandler(Process):
    """
    Work horse - get a request turns it into a
    `Requests` like object - fires it.
    """

    def __init__(self, queue, results):
        """
        :param queue: Work queue - items to work in.
        :param results: Responses - for processing and auditing.
        """
        super().__init__()
        self.work_queue = queue
        self.result_queue = results
        self.logger = logging.getLogger("load_test_worker")

    def run(self):
        """
        Main loop, in an asyncio loop.
        """
        try:
            self.logger.info("Starting...")
            asyncio.run(self.main_loop())
        except KeyboardInterrupt:
            self.logger.info("Exiting...")
            self.close()

    def prepare_request(self, request_data):
        """
        Create a request from a dictionary
        """
        payload = request_data.get("payload")
        self.prepare_payload(payload)
        request = Request(
            request_data.get("method"),
            request_data.get("path"),
            headers=request_data.get("headers"),
            json=payload
        )
        self.logger.debug("Sending {} to {}".format(
            payload, request_data.get("path")
        ))
        return request

    def prepare_payload(self, payload):
        """
        :param payload: If you find a `dynamic` field - punt the values in.
        """
        for key, value in payload.items():
            if value in DYNAMIC_MAP:
                payload[key] = DYNAMIC_MAP.get(value)()

    async def main_loop(self):
        """
        Main async loop

        Pop a "job" out (request in dict form) (if None, exit, we're done.)

        Turn the dict into httpx.Request and send it.

        Not sure I need/make use of Async Client here
         - might've accidentally coded around it.

        Add response and expected json data to queue for auditing.

        """
        while True:
            async with AsyncClient() as client:
                job = self.work_queue.get()
                if job is None:
                    return
                request_data, expected_response = job
                request = self.prepare_request(request_data)
                try:
                    sent = time.time()
                    response = await client.send(request)
                    recieved = time.time()
                    time_taken = recieved - sent
                    self.result_queue.put(
                        (
                            response,
                            expected_response,
                            time_taken
                        ))
                except httpx.RequestError as exc:
                    self.logger.info(
                        "An error occurred while requesting {}".format(exc))
                except httpx.HTTPStatusError as exc:
                    self.logger.info(
                        "Error response {} while requesting.".format(
                            exc))
