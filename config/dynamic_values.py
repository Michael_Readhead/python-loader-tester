from datetime import datetime, timezone
from multiprocessing import Value

TOTAL_REQUESTS_PER_SESSION = Value('i', 0)


def time_now():
    return datetime.now(timezone.utc).isoformat()


def requests_per_session():
    with TOTAL_REQUESTS_PER_SESSION.get_lock():
        TOTAL_REQUESTS_PER_SESSION.value += 1
    return str(TOTAL_REQUESTS_PER_SESSION.value)


DYNAMIC_MAP = {
    "dynamic{time_now}": time_now,
    "dynamic{requests_per_session}": requests_per_session,
}
