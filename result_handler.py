from collections import defaultdict
from multiprocessing import Process
import time
import logging
import httpx


class ResultHandler(Process):
    """
    Result handler - check and audit respsonses
    """

    def __init__(self, result_queue, target_rps=None):
        """
        Construcutor.

        Some times are initialised here but
        re-initialised later to avoid `None`

        Tests seem fine, should check with unittests, but don't have time.

        :param result_queue: A queue of responses for checking and auditing.
        :param target_rps:
            Sadly needed for auditing,
            needs refactoring for multiple targets.
        """
        super().__init__()
        self.result_queue = result_queue
        self.logger = logging.getLogger("results")
        self.total = 0
        self.success = 0
        self.errors = 0
        self.target_rps = target_rps
        self.start_time = time.time()
        self.status_tally = defaultdict(int)
        self.status_tally_times = defaultdict(float)

    def audit(self):
        """
        Popped out for ease of reading.
        """
        self.total += 1
        if self.total % 10 == 0:
            seconds_passed = time.time() - self.start_time
            req_per_sec = round(self.total / (seconds_passed), 2)
            self.logger.info("Approx req per sec {} (Target {})"
                             .format(req_per_sec, self.target_rps))

    def post_results(self):
        """
        Called (hopefully) after everything
        else has shutdown to post results.
        """
        seconds_passed = time.time() - self.start_time
        req_per_sec = round(self.total / (seconds_passed), 2)
        self.logger.info("Finished test")
        self.logger.info("Approximate RPS {}".format(req_per_sec))
        self.logger.info("Successful responses {}".format(self.success))
        self.logger.info("Error responses {}".format(self.errors))
        for status_code, tally in self.status_tally.items():
            cumalitive_time = self.status_tally_times.get(status_code)
            self.logger.info(
                "Status Code: {} - Count: {} Mean Time {}s".format(
                    status_code,
                    tally,
                    cumalitive_time / tally
                ))

    def handle_response(self, job):
        response, expected, time_taken = job
        self.audit()
        self.status_tally[response.status_code] += 1
        self.status_tally_times[response.status_code] += time_taken
        response.raise_for_status()
        try:
            if response.json() == expected:
                self.logger.debug("Expected response recieved")
                self.success += 1
            else:
                self.logger.warning(
                    "Incorrect Json Received {}".format(
                        response.content
                    ))
                self.errors += 1
        except Exception as e:
            self.logger.error(e)
            self.errors += 1

    def close(self):
        self.logger.warning("Exiting...")
        self.post_results()
        super().close()

    def run(self):
        """
        Main loop - don't like the nesting, but it works.
        """
        self.start_time = time.time()
        job = None
        while True:
            try:
                job = self.result_queue.get()
                if job is None:
                    self.close()
                    return
                self.handle_response(job)
            except httpx.HTTPStatusError as exc:
                self.logger.warning(
                    "Received unexpected response {}".format(exc))
                self.errors += 1
            except KeyboardInterrupt:
                break
        while job is not None:
            self.handle_response(job)
            job = self.result_queue.get()
        self.close()
