#!/usr/bin/env python

from random import randint

from flask import Flask, abort

app = Flask(__name__)

"""
Quick and dirty test bed.
"""


@app.route("/", methods=["POST"])
def handle_post():
    random_number = randint(0, 10)
    if random_number > 8:
        return abort(400)
    return {"successful": True}
