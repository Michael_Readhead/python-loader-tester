#!/usr/bin/env python

import logging
import signal

import multiprocessing
from multiprocessing import Queue
import multiprocessing_logging

from producer import Producer
from worker import LoadTestHandler
from result_handler import ResultHandler

from config import CONFIG

# Used to have a self made version of this - assuming this one will be better
# So far - so good.
multiprocessing_logging.install_mp_handler()

# CPU Count for Procesors
CPU_COUNT = multiprocessing.cpu_count()
# Handle exit cleaner.
signal.signal(signal.SIGINT, signal.default_int_handler)

"""
Quick and dirty load tester in python.

All config in `config` directory in python.

"""


def create_queue():
    """
    Popped out here for ease change to Joinable queue,
    if needed.
    """
    queue = Queue()
    return queue


def prepare_workers(work_queue, results_queue):
    """
    No sure this is the best idea in the world - but my thinking
    is one "CPU" is possible is going to be responsible for
    results counting and logging. The rest are for sending load.

    :param work_queue: A queue of request data, for sending out.
    :param results_queue:
        Once responses are acquired throw them in there for
        post processing.

    :return: A list of worker `Processes`, ready to start.

    """
    total_workers = CPU_COUNT - 1

    if total_workers < 0:
        total_workers += 1

    workers = []
    for i in range(total_workers):
        workers.append(
            LoadTestHandler(work_queue, results_queue)
        )
    return workers


def create_producer(work_queue):
    """
    Assmuing one process can create enough request data for sending out.
    (Local tests seeem to bear that out.)

    Config assumed to be global and unique
    this has nearly been refactored to run over multiple paths.
    Not needed by spec however.

    :param work_queue:
        Queue which the Producer
        loads requests for sending into.

    :return: Producer create
        requests as per config - and loads them into
        the work queue.
    """
    return Producer(queue=work_queue, config=CONFIG)


def main():
    """
    Entry point, normally I'd add some arg parsing but,
    this works for now, and the config file.
    """
    logging.basicConfig(level="INFO")
    work_queue = create_queue()
    results_queue = create_queue()
    workers = prepare_workers(work_queue, results_queue)
    producer = create_producer(work_queue)
    results = ResultHandler(results_queue, target_rps=producer.req_per_sec)
    try:
        results.start()
        [w.start() for w in workers]
        producer.start()
        while True:
            pass
    except KeyboardInterrupt:
        [work_queue.put(None) for w in workers]
        [w.join() for w in workers]
        results_queue.put(None)
        producer.join()
        results.join()


if __name__ == "__main__":
    main()
